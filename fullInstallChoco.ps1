Write-Host "Install Chocolate" -ForegroundColor green
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
$chocoapps = @('vlc','7zip','teamviewer','notepadplusplus','winrar','foxitreader','putty','gimp','k-litecodecpackfull','dropbox','winscp','spotify','putty','wireshark','postman','opera','signal','crystaldiskinfo','greenshot','openvpn','blender','vnc-viewer','python311','evernote','telegram','zip','speedtest','rocketchat','ringcentral-classic','discord','eclipse','bitwarden','keepassxc','mobaxterm','peazip','zoiper','sharex','far','sumatrapdf','shotcut','kitty','winfsp','meteor','handbrake','sublimetext2','mp3tag','viber','smartftp','consul','mremoteng','mpv','imgburn','hwmonitor','screenpresso','flutter','bleachbit','unchecky','nitroreader','wechat','plex','xnviewmp','teamspeak','bitvise-ssh-client')
$selectedApps = Get-Random -InputObject $chocoapps -Count 6
For ($i=0; $i -lt $selectedApps.Length; $i++) {
 Write-Host "Instaling..." $selectedApps[$i] -ForegroundColor green
 choco install $selectedApps[$i] -y
}

